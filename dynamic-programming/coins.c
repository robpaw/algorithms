
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <time.h>

#define min(a, b) (((a) < (b)) ? (a) : (b))

static const int C[] = {1, 2, 5, 10, 0};

int count_coins_naive(int s) {
  int i, k = INT_MAX - 1;

  if (s < 0)
    return INT_MAX - 1;
  if (s == 0)
    return 0;

  i = 0;
  while (C[i] > 0) {
    int tmp = count_coins_naive(s - C[i]) + 1;
    k = min(k, tmp);
    ++i;
  }
  return k;
}

int count_coins_dm(int s) {
  int *a, i;

  a = alloca(sizeof(int) * (s + 1));
  if (a == NULL) {
    return -1;
  }

  a[0] = 0;
  for (i = 1; i <= s; ++i) {
    int k;

    a[i] = INT_MAX - 1;

    k = 0;
    while (C[k] > 0 && i - C[k] >= 0) {
      int tmp = a[i - C[k]] + 1;
      a[i] = min(a[i], tmp);
      ++k;
    }
  }
  return a[s];
}

int main(int argc, char **argv) {
  int sum, i;

  printf("Enter total sum of coins: ");
  if (scanf("%d", &sum) != 1) {
    printf("Invalid value of the total sum.\n");
    return EXIT_FAILURE;
  }

  printf("The total sum | #coins naive |    #coins dm | time naive [ms] |    "
         "time dm [ms]\n");
  for (i = 0; i <= sum; ++i) {
    clock_t t1, t2;
    int r1, r2;

    t1 = clock();
    r1 = count_coins_naive(i);
    t1 = clock() - t1;

    t2 = clock();
    r2 = count_coins_dm(i);
    t2 = clock() - t2;

    printf("%13d | %12d | %12d | %15.3f | %15.3f\n", i, r1, r2,
           t1 / (double)CLOCKS_PER_SEC * 1000.0,
           t2 / (double)CLOCKS_PER_SEC * 1000.0);

    assert(r1 == r2);
  }

  return EXIT_SUCCESS;
}
