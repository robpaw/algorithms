
/*

Problem Statement

The old song declares "Go ahead and hate your neighbor", and the residents of
Onetinville have taken those words to heart. Every resident hates his next-door
neighbors on both sides. Nobody is willing to live farther away from the town's
well than his neighbors, so the town has been arranged in a big circle around
the well. Unfortunately, the town's well is in disrepair and needs to be
restored. You have been hired to collect donations for the Save Our Well fund.

Each of the town's residents is willing to donate a certain amount, as specified
in the int[] donations, which is listed in clockwise order around the well.
However, nobody is willing to contribute to a fund to which his neighbor has
also contributed. Next-door neighbors are always listed consecutively in
donations, except that the first and last entries in donations are also for
next-door neighbors. You must calculate and return the maximum amount of
donations that can be collected.

Definition

Class:	BadNeighbors
Method:	maxDonations
Parameters:	int[]
Returns:	int
Method signature:	int maxDonations(int[] donations)
(be sure your method is public)

Constraints
-	donations contains between 2 and 40 elements, inclusive.
-	Each element in donations is between 1 and 1000, inclusive.

Examples

0)
 { 10, 3, 2, 5, 7, 8 }
Returns: 19
The maximum donation is 19, achieved by 10+2+7. It would be better to take
10+5+8 except that the 10 and 8 donations are from neighbors.

1)
{ 11, 15 }
Returns: 15

2)
{ 7, 7, 7, 7, 7, 7, 7 }
Returns: 21

3)
{ 1, 2, 3, 4, 5, 1, 2, 3, 4, 5 }
Returns: 16

4)
{ 94, 40, 49, 65, 21, 21, 106, 80, 92, 81, 679, 4, 61,
  6, 237, 12, 72, 74, 29, 95, 265, 35, 47, 1, 61, 397,
  52, 72, 37, 51, 1, 81, 45, 435, 7, 36, 57, 86, 81, 72 }
Returns: 2926

This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <cassert>

class BadNeighbors0 {
public:
  int maxDonations(const std::vector<int> &d, int b, int n) {
    if (n == 0)
      return 0;
    int v = d[b % d.size()], maxv = v;
    for (int i = 2; i <= n; ++i) {
      maxv = std::max(maxv, v + maxDonations(d, b + i, n - i));
    }
    return maxv;
  }
  int maxDonations(const std::vector<int> &d) {
    int v = 0;
    for (int i = 0; i < (int)d.size(); ++i) {
      v = std::max(v, maxDonations(d, i, d.size() - 1));
    }
    return v;
  }
};

class BadNeighbors1 {
public:
  int maxDonations(const std::vector<int> &d, int b) {
    const int s = d.size();

    if (s == 0)
      return 0;
    if (s <= 3)
      return d[b];

    std::vector<int> tmp(s);

    tmp[0] = d[b];
    tmp[1] = 0;
    tmp[2] = tmp[0] + d[(b + 2) % s];
    for (int i = 3; i < (s - 1); ++i) {
      tmp[i] = std::max(tmp[i - 3], tmp[i - 2]) + d[(b + i) % s];
    }
    return *std::max_element(tmp.begin(), tmp.end());
  }
  int maxDonations(const std::vector<int> &d) {
    int v = 0;
    int n = std::min(3, (int)d.size());
    for (int i = 0; i < n; ++i) {
      v = std::max(v, maxDonations(d, i));
    }

    return v;
  }
};

const std::vector<int> data[5][2] = {
    {{10, 3, 2, 5, 7, 8}, {19}},
    {{11, 15}, {15}},
    {{7, 7, 7, 7, 7, 7, 7}, {21}},
    {{1, 2, 3, 4, 5, 1, 2, 3, 4, 5}, {16}},
    {{94, 40, 49, 65, 21, 21, 106, 80, 92, 81, 679, 4, 61, 6, 237, 12, 72, 74,
      29, 95, 265, 35, 47, 1, 61, 397, 52, 72, 37, 51, 1, 81, 45, 435, 7, 36,
      57, 86, 81, 72},
     {2926}}

};

int main(int argc, char **argv) {
  BadNeighbors1 b;
  for (int i = 0; i < 5; ++i) {
    auto r1 = data[i][1][0];
    auto r2 = b.maxDonations(data[i][0]);

    std::cout << "r1: " << r1 << std::endl;
    std::cout << "r2: " << r2 << std::endl;

    assert(r1 == r2);
  }

  return 0;
}
