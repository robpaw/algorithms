
/*

Problem Statement for QuickSums

Problem Statement
        Given a string of digits, find the minimum number of additions required
for the string to equal some target number. Each addition is the equivalent of
inserting a plus sign somewhere into the string of digits. After all plus signs
are inserted, evaluate the sum as usual. For example, consider the string "12"
(quotes for clarity). With zero additions, we can achieve the number 12. If we
insert one plus sign into the string, we get "1+2", which evaluates to 3. So, in
that case, given "12", a minimum of 1 addition is required to get the number 3.
As another example, consider "303" and a target sum of 6. The best strategy is
not "3+0+3", but "3+03". You can do this because leading zeros do not change the
result.
Write a class QuickSums that contains the method minSums, which takes a String
numbers and an int sum. The method should calculate and return the minimum
number of additions required to create an expression from numbers that evaluates
to sum. If this is impossible, return -1.

Definition

Class:	QuickSums
Method:	minSums
Parameters:	String, int
Returns:	int
Method signature:	int minSums(String numbers, int sum)
(be sure your method is public)

Constraints
-	numbers will contain between 1 and 10 characters, inclusive.
-	Each character in numbers will be a digit.
-	sum will be between 0 and 100, inclusive.

Examples

0)
"99999"
45
Returns: 4
In this case, the only way to achieve 45 is to add 9+9+9+9+9. This requires 4
additions.

1)
"1110"
3
Returns: 3
Be careful with zeros. 1+1+1+0=3 and requires 3 additions.

2)
"0123456789"
45
Returns: 8

3)
"99999"
100
Returns: -1

4)
"382834"
100
Returns: 2
There are 3 ways to get 100. They are 38+28+34, 3+8+2+83+4 and 3+82+8+3+4. The
minimum required is 2.

5)
"9230560001"
71
Returns: 4


This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>

#include <cassert>

class QuickSums {

public:
  int minSums(const std::string &str, int sum) {
    int size = str.size() + 1;

    std::vector<int> dig(size);
    for (int i = 0; i < (int)str.size(); ++i) {
      dig[i + 1] = str[i] - '0';
    }

    std::vector<int> arr((sum + 1) * size, size);
    auto get = [&](int s, int i) -> int & { return arr[s * size + i]; };

    get(0, 0) = -1;
    for (int i = 1; i < size; ++i) {
      int num = 0;
      for (int j = i; j < size && num < sum; ++j) {
        num *= 10;
        num += dig[j];
        for (int k = 0; k + num <= sum; ++k) {
          get(k + num, j) = std::min(get(k + num, j), get(k, i - 1) + 1);
        }
      }
    }
    int val = get(sum, size - 1);
    return (val == size) ? -1 : val;
  }
};

struct example {
  std::string str;
  int sum;
  int res;
};

static const std::vector<example> data = {
    {"99999", 45, 4},   {"1110", 3, 3},     {"0123456789", 45, 8},
    {"99999", 100, -1}, {"382834", 100, 2}, {"9230560001", 71, 4}};

int main(int argc, char **argv) {
  for (const auto &e : data) {
    QuickSums s;

    auto res1 = e.res;
    auto res2 = s.minSums(e.str, e.sum);

    std::cout << "res1: " << res1 << std::endl;
    std::cout << "res2: " << res2 << std::endl;

    assert(res1 == res2);
  }
  return 0;
}
