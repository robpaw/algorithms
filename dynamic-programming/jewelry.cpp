
/*

Problem Statement for Jewelry

Problem Statement
You have been given a list of jewelry items that must be split amongst two
people: Frank and Bob. Frank likes very expensive jewelry. Bob doesn't care how
expensive the jewelry is, as long as he gets a lot of jewelry. Based on these
criteria you have devised the following policy:
1) Each piece of jewelry given to Frank must be valued greater than or equal to
each piece of jewelry given to Bob. In other words, Frank's least expensive
piece of jewelry must be valued greater than or equal to Bob's most expensive
piece of jewelry.
2) The total value of the jewelry given to Frank must exactly equal the total
value of the jewelry given to Bob.
3) There can be pieces of jewelry given to neither Bob nor Frank.
4) Frank and Bob must each get at least 1 piece of jewelry.
Given the value of each piece, you will determine the number of different ways
you can allocate the jewelry to Bob and Frank following the above policy. For
example:
        values = {1,2,5,3,4,5}
Valid allocations are:
  Bob       		Frank
  1,2		         3
  1,3        		 4
  1,4		         5  (first 5)
  1,4         		 5  (second 5)
  2,3 		         5  (first 5)
  2,3         		 5  (second 5)
   5  (first 5)		 5  (second 5)
   5  (second 5)	 5  (first 5)
1,2,3,4       		5,5
Note that each '5' is a different piece of jewelry and needs to be accounted for
separately. There are 9 legal ways of allocating the jewelry to Bob and Frank
given the policy, so your method would return 9.

Definition

Class:	Jewelry
Method:	howMany
Parameters:	int[]
Returns:	long
Method signature:	long howMany(int[] values)
(be sure your method is public)


Constraints
-	values will contain between 2 and 30 elements inclusive.
-	Each element of values will be between 1 and 1000 inclusive.

Examples

0)
{1,2,5,3,4,5}
Returns: 9
From above.

1)
{1000,1000,1000,1000,1000,
 1000,1000,1000,1000,1000,
 1000,1000,1000,1000,1000,
 1000,1000,1000,1000,1000,
 1000,1000,1000,1000,1000,
 1000,1000,1000,1000,1000}
Returns: 18252025766940

2)
{1,2,3,4,5}
Returns: 4
Valid allocations:
Bob               Frank
1,2                 3
2,3                 5
1,3                 4
1,4                 5
3)

{7,7,8,9,10,11,1,2,2,3,4,5,6}
Returns: 607

4)
{123,217,661,678,796,964,54,111,417,526,917,923}
Returns: 0

This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>
#include <numeric>

#include <cassert>

class Jewelry {

  /*
   * The knapsack problem
   */
  template <typename Iterator>
  void getWays(std::vector<long> &ways, Iterator from, Iterator to) {
    int sum = std::accumulate(from, to, 0);
    std::fill(ways.begin(), ways.end(), 0);
    ways[0] = 1;
    while (from != to) {
      int v = *from;
      for (int s = sum; s >= v; --s) {
        ways[s] += ways[s - v];
      }
      ++from;
    }
  }

  long combinations(int n, int k) {
    assert(k >= 0);
    assert(n >= 0);

    if (k > n)
      return 0;
    if (k > n / 2) {
      k = n - k;
    }

    long c = 1;
    for (long i = 1; i <= k; ++i) {
      c *= (n + 1 - i);
      c /= i;
    }
    return c;
  }

public:
  long howMany(std::vector<int> values) {
    std::sort(values.begin(), values.end());

    int sum = std::accumulate(values.begin(), values.end(), 0);

    std::vector<long> below(sum + 1), above(sum + 1);

    auto b = values.begin();
    auto e = values.end();

    long res = 0;
    while (b != e) {
      int v = *b;

      // look for a group of equal elements
      auto p = std::next(b, 1);
      while (*p == v && p != e) {
        ++p;
      }
      int gsize = std::distance(b, p);

      getWays(below, values.begin(), b);
      for (int g = 1; g <= gsize; ++g) {
        getWays(above, std::next(b, g), values.end());
        long c = combinations(gsize, g);
        for (int s = g * v; s <= sum; ++s) {
          res += c * below[s - g * v] * above[s];
        }
      }
      b = p;
    }
    return res;
  }
};

struct example {
  std::vector<int> values;
  long res;
};

static const std::vector<example> data{
    {{1, 2, 5, 3, 4, 5}, 9},
    {{1, 2, 3, 4, 5}, 4},
    {{7, 7, 8, 9, 10, 11, 1, 2, 2, 3, 4, 5, 6}, 607},
    {{123, 217, 661, 678, 796, 964, 54, 111, 417, 526, 917, 923}, 0},
    {{1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000,
      1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000,
      1000, 1000, 1000, 1000, 1000, 1000},
     18252025766940}};

int main(int argc, char **argv) {
  for (const auto &e : data) {
    Jewelry j;

    auto res1 = e.res;
    auto res2 = j.howMany(e.values);

    std::cout << "res1: " << res1 << std::endl;
    std::cout << "res2: " << res2 << std::endl;

    assert(res1 == res2);
  }
  return 0;
}
