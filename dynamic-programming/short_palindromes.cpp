
/*

Problem Statement for ShortPalindromes

Problem Statement

A palindrome is a String that is spelled the same forward and backwards. Given a
String base that may or may not be a palindrome, we can always force base to be
a palindrome by adding letters to it. For example, given the word "RACE", we
could add the letters "CAR" to its back to get "RACECAR" (quotes for clarity
only). However, we are not restricted to adding letters at the back. For
example, we could also add the letters "ECA" to the front to get "ECARACE". In
fact, we can add letters anywhere in the word, so we could also get "ERCACRE" by
adding an 'E' at the beginning, a 'C' after the 'R', and another 'R' before the
final 'E'. Your task is to make base into a palindrome by adding as few letters
as possible and return the resulting String. When there is more than one
palindrome of minimal length that can be made, return the lexicographically
earliest (that is, the one that occurs first in alphabetical order).

Definition

Class:	ShortPalindromes
Method:	shortest
Parameters:	String
Returns:	String
Method signature:	String shortest(String base)
(be sure your method is public)


Constraints
-	base contains between 1 and 25 characters, inclusive.
-	Every character in base is an uppercase letter ('A'-'Z').

Examples

0)
"RACE"
Returns: "ECARACE"
To make "RACE" into a palindrome, we must add at least three letters. However,
there are eight ways to do this by adding exactly three letters:
    "ECARACE"  "ECRARCE"  "ERACARE"  "ERCACRE"
    "RACECAR"  "RAECEAR"  "REACAER"  "RECACER"
Of these alternatives, "ECARACE" is the lexicographically earliest.

1)
"TOPCODER"
Returns: "REDTOCPCOTDER"

2)
"Q"
Returns: "Q"

3)
"MADAMIMADAM"
Returns: "MADAMIMADAM"

4)
"ALRCAGOEUAOEURGCOEUOOIGFA"
Returns: "AFLRCAGIOEOUAEOCEGRURGECOEAUOEOIGACRLFA"

This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>

#include <cassert>

class ShortPalindromes {

public:
  std::string shortest(const std::string &str) {
#if 1
    // DP solution
    int size = str.size();

    std::vector<std::string> arr(size * size);
    auto get = [&](int x, int y) -> std::string & { return arr[y * size + x]; };

    for (int i = 0; i < size; ++i) {
      get(i, i) = str[i];
    }

    for (int i = 1; i < size; ++i) {
      for (int j = 0; j + i < size; ++j) {
        int b = j, e = j + i;
        if (get(b, b) == get(e, e)) {
          if (i > 1) {
            get(b, e) = get(b, b) + get(b + 1, e - 1) + get(e, e);
          } else {
            get(b, e) = get(b, b) + get(e, e);
          }
        } else {
          const auto &s1 = get(b + 1, e);
          const auto &s2 = get(b, e - 1);
          if ((s1.size() == s2.size() && get(b, b) < get(e, e)) ||
              s1.size() < s2.size()) {
            get(b, e) = get(b, b) + s1 + get(b, b);
          } else {
            get(b, e) = get(e, e) + s2 + get(e, e);
          }
        }
      }
    }
    return get(0, size - 1);
#else
    std::string res = str;
    if (str.size() > 1) {
      if (str.back() == str.front()) {
        auto tmp = std::string(std::next(str.begin()), std::prev(str.end()));
        res = str.front() + shortest(tmp) + str.back();
      } else {
        auto str1 = shortest(std::string(std::next(str.begin()), str.end()));
        auto str2 = shortest(std::string(str.begin(), std::prev(str.end())));
        if ((str1.size() == str2.size() && str.front() < str.back()) ||
            str1.size() < str2.size()) {
          res = str.front() + str1 + str.front();
        } else {
          res = str.back() + str2 + str.back();
        }
      }
    }
    assert(str.size() <= res.size());
    return res;
#endif
  }
};

struct example {
  std::string str, res;
};

static const std::vector<example> data = {
    {"RACE", "ECARACE"},
    {"TOPCODER", "REDTOCPCOTDER"},
    {"Q", "Q"},
    {"MADAMIMADAM", "MADAMIMADAM"},
    {"ALRCAGOEUAOEURGCOEUOOIGFA", "AFLRCAGIOEOUAEOCEGRURGECOEAUOEOIGACRLFA"}};

int main(int argc, char **argv) {
  for (const auto &e : data) {
    ShortPalindromes s;

    auto res1 = e.res;
    auto res2 = s.shortest(e.str);

    std::cout << "res1: " << res1 << std::endl;
    std::cout << "res2: " << res2 << std::endl;

    assert(res1 == res2);
  }
  return 0;
}
