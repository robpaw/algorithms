
/*

Problem Statement
Problem contains images. Plugin users can view them in the applet.

In the city, roads are arranged in a grid pattern. Each point on the grid
represents a corner where two blocks meet. The points are connected by line
segments which represent the various street blocks. Using the cartesian
coordinate system, we can assign a pair of integers to each corner as shown
below.


You are standing at the corner with coordinates 0,0. Your destination is at
corner width,height. You will return the number of distinct paths that lead to
your destination. Each path must use exactly width+height blocks. In addition,
the city has declared certain street blocks untraversable. These blocks may not
be a part of any path. You will be given a String[] bad describing which blocks
are bad. If (quotes for clarity) "a b c d" is an element of bad, it means the
block from corner a,b to corner c,d is untraversable. For example, let's say
width  = 6
length = 6
bad = {"0 0 0 1","6 6 5 6"}
The picture below shows the grid, with untraversable blocks darkened in black. A
sample path has been highlighted in red.

Definition

Class:	AvoidRoads
Method:	numWays
Parameters:	int, int, String[]
Returns:	long
Method signature:	long numWays(int width, int height, String[] bad)
(be sure your method is public)

Constraints
-	width will be between 1 and 100 inclusive.
-	height will be between 1 and 100 inclusive.
-	bad will contain between 0 and 50 elements inclusive.
-	Each element of bad will contain between 7 and 14 characters inclusive.
-	Each element of the bad will be in the format "a b c d" where,
a,b,c,d are integers with no extra leading zeros,
a and c are between 0 and width inclusive,
b and d are between 0 and height inclusive,
and a,b is one block away from c,d.
-	The return value will be between 0 and 2^63-1 inclusive.

Examples

0)

6
6
{"0 0 0 1","6 6 5 6"}
Returns: 252
Example from above.

1)

1
1
{}
Returns: 2
Four blocks aranged in a square. Only 2 paths allowed.

2)

35
31
{}
Returns: 6406484391866534976
Big number.

3)

2
2
{"0 0 1 0", "1 2 2 2", "1 1 2 1"}
Returns: 0

This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>

#include <cassert>

class AvoidRoads {
  struct edge {
    int x1, y1, x2, y2;
    bool operator==(const edge &o) const {
      return x1 == o.x1 && x2 == o.x2 && y1 == o.y1 && y2 == o.y2;
    }
  };

public:
  long long numWays(int w, int h, const std::vector<std::string> &bad) {
    // the problem can be solved with O(min(w, h)) memory
    // -> one needs to store only the current and the previous row/column
    std::vector<long long> roads((w + 1) * (h + 1));
    std::vector<edge> bad2(bad.size());

    for (int i = 0; i < (int)bad.size(); ++i) {
      std::istringstream str(bad[i]);
      str >> bad2[i].x1 >> bad2[i].y1 >> bad2[i].x2 >> bad2[i].y2;
    };

    auto is_bad = [&](int fromx, int fromy, int tox, int toy) -> bool {
      // check transition in both directions
      const edge e1 = {.x1 = fromx, .y1 = fromy, .x2 = tox, .y2 = toy};
      const edge e2 = {.x1 = tox, .y1 = toy, .x2 = fromx, .y2 = fromy};
      return std::find(bad2.begin(), bad2.end(), e1) != bad2.end() ||
             std::find(bad2.begin(), bad2.end(), e2) != bad2.end();
    };

    auto node = [&](int x, int y) -> long long & {
      return roads[y * (w + 1) + x];
    };

    // init the first row/column
    node(0, 0) = 1;
    for (int x = 1; x <= w; ++x) {
      node(x, 0) = is_bad(x - 1, 0, x, 0) ? 0 : node(x - 1, 0);
    }
    for (int y = 1; y <= h; ++y) {
      node(0, y) = is_bad(0, y - 1, 0, y) ? 0 : node(0, y - 1);
    }

    for (int y = 1; y <= h; ++y) {
      for (int x = 1; x <= w; ++x) {
        long long xx = is_bad(x - 1, y, x, y) ? 0 : (node(x - 1, y));
        long long yy = is_bad(x, y - 1, x, y) ? 0 : (node(x, y - 1));
        node(x, y) = xx + yy;
      }
    }

    return *roads.rbegin();
  }
};

struct example {
  int w, h;
  std::vector<std::string> bad;
  long long res;
};

static const std::vector<example> data{
    {6, 6, {"0 0 0 1", "6 6 5 6"}, 252},
    {1, 1, {}, 2},
    {35, 31, {}, 6406484391866534976},
    {2, 2, {"0 0 1 0", "1 2 2 2", "1 1 2 1"}, 0}};

int main(int argc, char **argv) {
  for (const auto &e : data) {
    AvoidRoads a;

    auto res1 = e.res;
    auto res2 = a.numWays(e.w, e.h, e.bad);

    std::cout << "res1: " << res1 << std::endl;
    std::cout << "res2: " << res2 << std::endl;

    assert(res1 == res2);
  }
  return 0;
}
