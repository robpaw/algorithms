
/*

Problem Statement
Suppose you had an n by n chess board and a super piece called a kingknight.
Using only one move the kingknight denoted 'K' below can reach any of the spaces
denoted 'X' or 'L' below:
   .......
   ..L.L..
   .LXXXL.
   ..XKX..
   .LXXXL.
   ..L.L..
   .......
In other words, the kingknight can move either one space in any direction
(vertical, horizontal or diagonally) or can make an 'L' shaped move. An 'L'
shaped move involves moving 2 spaces horizontally then 1 space vertically or 2
spaces vertically then 1 space horizontally. In the drawing above, the 'L'
shaped moves are marked with 'L's whereas the one space moves are marked with
'X's. In addition, a kingknight may never jump off the board.

Given the size of the board, the start position of the kingknight and the end
position of the kingknight, your method will return how many possible ways there
are of getting from start to end in exactly numMoves moves. start and finish are
int[]s each containing 2 elements. The first element will be the (0-based) row
position and the second will be the (0-based) column position. Rows and columns
will increment down and to the right respectively. The board itself will have
rows and columns ranging from 0 to size-1 inclusive.

Note, two ways of getting from start to end are distinct if their respective
move sequences differ in any way. In addition, you are allowed to use spaces on
the board (including start and finish) repeatedly during a particular path from
start to finish. We will ensure that the total number of paths is less than or
equal to 2^63-1 (the upper bound for a long).

Definition

Class:	ChessMetric
Method:	howMany
Parameters:	int, int[], int[], int
Returns:	long
Method signature:	long howMany(int size, int[] start, int[] end, int
numMoves)
(be sure your method is public)


Notes
-	For C++ users: long long is a 64 bit datatype and is specific to the
GCC compiler.

Constraints
-	size will be between 3 and 100 inclusive
-	start will contain exactly 2 elements
-	finish will contain exactly 2 elements
-	Each element of start and finish will be between 1 and size-1 inclusive
-	numMoves will be between 1 and 50 inclusive
-	The total number of paths will be at most 2^63-1.

Examples

0)

3
{0,0}
{1,0}
1
Returns: 1
Only 1 way to get to an adjacent square in 1 move

1)

3
{0,0}
{1,2}
1
Returns: 1
A single L-shaped move is the only way

2)

3
{0,0}
{2,2}
1
Returns: 0
Too far for a single move

3)

3
{0,0}
{0,0}
2
Returns: 5
Must count all the ways of leaving and then returning to the corner

4)

100
{0,0}
{0,99}
50
Returns: 243097320072600


This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>

#include <cassert>

class ChessMetric {
public:
  long long howMany(int size, const std::vector<int> &start,
                    const std::vector<int> &end, int numMoves) {
    std::vector<long long> b1(size * size), b2(size * size);

    auto is_valid = [=](int x, int y) -> bool {
      return x >= 0 && x < size && y >= 0 && y < size;
    };

    auto field = [&](std::vector<long long> &b, int x, int y) -> long long & {
      assert(is_valid(x, y));
      return b[y * size + x];
    };

    field(b1, start[0], start[1]) = 1;
    for (int n = 0; n < numMoves; ++n) {
      for (int y = 0; y < size; ++y) {
        for (int x = 0; x < size; ++x) {
          static const int moves[][2] = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1},
                                         {0, 1},   {1, -1}, {1, 0},  {1, 1},
                                         {-1, -2}, {-1, 2}, {1, -2}, {1, 2},
                                         {-2, -1}, {-2, 1}, {2, -1}, {2, 1}};
          long long val = 0;
          for (const auto &m : moves) {
            int xx = x + m[0];
            int yy = y + m[1];
            if (is_valid(xx, yy)) {
              val += field(b1, xx, yy);
            }
          }
          field(b2, x, y) = val;
        }
      }
      // one could swap pointers to avoid copying
      b1 = b2;
    }
    return field(b1, end[0], end[1]);
  }
};

struct example {
  int size;
  std::vector<int> start, end;
  int numMoves;
  long long res;
};

static const std::vector<example> data{
    {3, {0, 0}, {1, 0}, 1, 1},
    {3, {0, 0}, {1, 2}, 1, 1},
    {3, {0, 0}, {2, 2}, 1, 0},
    {3, {0, 0}, {0, 0}, 2, 5},
    {100, {0, 0}, {0, 99}, 50, 243097320072600}};

int main(int argc, char **argv) {
  for (const auto &e : data) {
    ChessMetric m;

    auto res1 = e.res;
    auto res2 = m.howMany(e.size, e.start, e.end, e.numMoves);

    std::cout << "res1: " << res1 << std::endl;
    std::cout << "res2: " << res2 << std::endl;

    assert(res1 == res2);
  }
  return 0;
}
