/*
Problem Statement

You are planting a flower garden with bulbs to give you joyous flowers
throughout the year. However, you wish to plant the flowers such that they do
not block other flowers while they are visible.

You will be given a int[] height, a int[] bloom, and a int[] wilt. Each type of
flower is represented by the element at the same index of height, bloom, and
wilt. height represents how high each type of flower grows, bloom represents the
morning that each type of flower springs from the ground, and wilt represents
the evening that each type of flower shrivels up and dies. Each element in bloom
and wilt will be a number between 1 and 365 inclusive, and wilt[i] will always
be greater than bloom[i]. You must plant all of the flowers of the same type in
a single row for appearance, and you also want to have the tallest flowers as
far forward as possible. However, if a flower type is taller than another type,
and both types can be out of the ground at the same time, the shorter flower
must be planted in front of the taller flower to prevent blocking. A flower
blooms in the morning, and wilts in the evening, so even if one flower is
blooming on the same day another flower is wilting, one can block the other.

You should return a int[] which contains the elements of height in the order you
should plant your flowers to acheive the above goals. The front of the garden is
represented by the first element in your return value, and is where you view the
garden from. The elements of height will all be unique, so there will always be
a well-defined ordering.

Definition

Class:	FlowerGarden
Method:	getOrdering
Parameters:	int[], int[], int[]
Returns:	int[]
Method signature:	int[] getOrdering(int[] height, int[] bloom, int[] wilt)
(be sure your method is public)

Constraints
-	height will have between 2 and 50 elements, inclusive.
-	bloom will have the same number of elements as height
-	wilt will have the same number of elements as height
-	height will have no repeated elements.
-	Each element of height will be between 1 and 1000, inclusive.
-	Each element of bloom will be between 1 and 365, inclusive.
-	Each element of wilt will be between 1 and 365, inclusive.
-	For each element i of bloom and wilt, wilt[i] will be greater than
bloom[i].

Examples

0)
{5,4,3,2,1}
{1,1,1,1,1}
{365,365,365,365,365}

Returns: { 1,  2,  3,  4,  5 }

These flowers all bloom on January 1st and wilt on December 31st. Since they all
may block each other, you must order them from shortest to tallest.

1)
{5,4,3,2,1}
{1,5,10,15,20}
{4,9,14,19,24}
Returns: { 5,  4,  3,  2,  1 }

The same set of flowers now bloom all at separate times. Since they will never
block each other, you can order them from tallest to shortest to get the tallest
ones as far forward as possible.

2)
{5,4,3,2,1}
{1,5,10,15,20}
{5,10,15,20,25}
Returns: { 1,  2,  3,  4,  5 }

Although each flower only blocks at most one other, they all must be ordered
from shortest to tallest to prevent any blocking from occurring.

3)
{5,4,3,2,1}
{1,5,10,15,20}
{5,10,14,20,25}
Returns: { 3,  4,  5,  1,  2 }

The difference here is that the third type of flower wilts one day earlier than
the blooming of the fourth flower. Therefore, we can put the flowers of height 3
first, then the flowers of height 4, then height 5, and finally the flowers of
height 1 and 2. Note that we could have also ordered them with height 1 first,
but this does not result in the maximum possible height being first in the
garden.

4)
{1,2,3,4,5,6}
{1,3,1,3,1,3}
{2,4,2,4,2,4}
Returns: { 2,  4,  6,  1,  3,  5 }

5)
{3,2,5,4}
{1,2,11,10}
{4,3,12,13}
Returns: { 4,  5,  2,  3 }

This problem statement is the exclusive and proprietary property of TopCoder,
Inc. Any unauthorized use or reproduction of this information without the prior
written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder,
Inc. All rights reserved.

*/

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <iterator>
#include <cassert>

class FlowerGarden {
public:
  std::vector<int> getOrdering(const std::vector<int> &height,
                               const std::vector<int> &bloom,
                               const std::vector<int> &wilt) {
    const int s = height.size();
    std::vector<int> o, p, r;
    std::list<int> q(s);

    auto overlap = [&](int i, int k) -> bool {
      return bloom[i] <= wilt[k] && bloom[k] <= wilt[i];
    };

    // set of remaining elements
    std::iota(q.begin(), q.end(), 0);

    o.reserve(s); // result
    p.reserve(s); // not-colliding heights
    r.reserve(s); // not-colliding indices

    while (!q.empty()) {
      p.clear();
      r.clear();

      // find all not-colliding flowers
      for (int a : q) {
        bool collide = false;
        for (int b : q) {
          if (a == b)
            continue;
          if (overlap(a, b) && height[a] > height[b]) {
            collide = true;
            break;
          }
        }
        if (!collide) {
          p.push_back(height[a]);
          r.push_back(a);
        }
      }

      int i = std::distance(p.begin(), std::max_element(p.begin(), p.end()));

      o.push_back(p[i]);
      q.remove(r[i]);
    }
    return o;
  }
};

class FlowerGarden2 {
public:
  std::vector<int> getOrdering(const std::vector<int> &height,
                               const std::vector<int> &bloom,
                               const std::vector<int> &wilt) {
    const int s = height.size();
    std::vector<std::vector<int>> m;

    // matrix representing a graph
    m.resize(s + 1);
    for (auto &mm : m) {
      mm.resize(s + 1);
    }

    auto overlap = [&](int i, int k) -> bool {
      return bloom[i] <= wilt[k] && bloom[k] <= wilt[i];
    };

    for (int i = 0; i < s; ++i) {
      for (int k = 0; k < s; ++k) {
        // no need to check i != k, height[i] < height[k] verifies it
        if (height[i] < height[k] && overlap(i, k)) {
          // transition i -> k
          m[i][k] = 1;

          ++m[i][s]; // #outputs
          ++m[s][k]; // #inputs
        }
      }
    }

    // the result
    std::vector<int> o;
    o.reserve(s);

    int n = s;
    while (n > 0) {

      // find the highest node with no inputs (no predecessors)
      int maxh = 0, maxi = -1;
      for (int i = 0; i < s; ++i) {
        if (m[s][i] == 0 && height[i] > maxh) {
          maxh = height[i];
          maxi = i;
        }
      }

      assert(maxh > 0);
      assert(maxi != -1);

      o.push_back(maxh);

      // mark the node as used
      m[s][maxi] = -1;

      // erase the node and its outputs
      for (int i = 0; i < s; ++i) {
        if (m[maxi][i] == 1) {
          --m[s][i];
        }
      }

      --n;
    }
    return o;
  }
};

std::vector<int> data[5][4] = {
    {{5, 4, 3, 2, 1}, {1, 5, 10, 15, 20}, {4, 9, 14, 19, 24}, {5, 4, 3, 2, 1}},
    {{5, 4, 3, 2, 1}, {1, 5, 10, 15, 20}, {5, 10, 15, 20, 25}, {1, 2, 3, 4, 5}},
    {{5, 4, 3, 2, 1}, {1, 5, 10, 15, 20}, {5, 10, 14, 20, 25}, {3, 4, 5, 1, 2}},
    {{1, 2, 3, 4, 5, 6},
     {1, 3, 1, 3, 1, 3},
     {2, 4, 2, 4, 2, 4},
     {2, 4, 6, 1, 3, 5}},
    {{3, 2, 5, 4}, {1, 2, 11, 10}, {4, 3, 12, 13}, {4, 5, 2, 3}}};

int main(int argc, char **argv) {
  FlowerGarden g1;
  FlowerGarden2 g2;
  for (int i = 0; i < 5; ++i) {
    const auto r1 = data[i][3];
    const auto r2 = g1.getOrdering(data[i][0], data[i][1], data[i][2]);
    const auto r3 = g2.getOrdering(data[i][0], data[i][1], data[i][2]);

    std::cout << "r1: ";
    std::copy(r1.begin(), r1.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::cout << "r2: ";
    std::copy(r2.begin(), r2.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::cout << "r3: ";
    std::copy(r3.begin(), r3.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    assert(std::equal(r1.begin(), r1.end(), r2.begin()));
    assert(std::equal(r1.begin(), r1.end(), r3.begin()));
  }
  return 0;
}
